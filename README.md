This image provides an environment to compile and run local FELIX tests inside a docker.
One can for instance on a Windows or Mac compile FELIX using this image while the source code
is stored and editable from the local system.

The LCG software system for use in FELIX is included in this image.

To use it, make sure you have docker installed and run on your MacOS host:

```
docker run -v `pwd`:/felix-master -it gitlab-registry.cern.ch/atlas-tdaq-felix/felix-build-image:el9
```

you will find your files under:

```
/felix-master
```

and lcg under:

```
/otp/lcg
```


Mark Donszelmann (duns@cern.ch)
